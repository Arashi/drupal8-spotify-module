<?php

namespace Drupal\spotify\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SpotifyForm extends ConfigFormBase {

  public function getFormId()
  {
    return 'spotify_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('spotify.settings');

    $form['artists_count'] = [
    '#type' => 'select',
    '#title' => $this
        ->t('Artists to display'),
    '#options' => array_combine(range(1, 20), range(1, 20)),
    '#default_value' => $config->get('spotify.artists_count'),
    '#description' => $this->t('Number of Artists to display in block.'),
    ];

    $form['spotify_ID'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Spotify Client ID:'),
      '#default_value' => $config->get('spotify.spotify_ID'),
      '#description' => $this->t('Find it in your app dashboard.'),
    );

    $form['spotify_secret'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Spotify Client Secret:'),
      '#default_value' => $config->get('spotify.spotify_secret'),
      '#description' => $this->t('Find it in your app dashboard.'),
    );

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $config = $this->config('spotify.settings');
    $config->set('spotify.artists_count', $form_state->getValue('artists_count'));
    $config->set('spotify.spotify_ID', $form_state->getValue('spotify_ID'));
    $config->set('spotify.spotify_secret', $form_state->getValue('spotify_secret'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  protected function getEditableConfigNames()
  {
    return [
      'spotify.settings',
    ];
  }

}
