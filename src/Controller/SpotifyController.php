<?php

namespace Drupal\spotify\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\Core\Url;
use Drupal\Core\Link;

class SpotifyController extends ControllerBase {

  protected function getModuleName()
  {
    return 'spotify';
  }

  /**
   * @param string $artist_id
   *   Spotify ID of author
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   If the parameters are invalid.
   */
  public function artistPage($artist_id)
  {
    $element['#markup'] = '';
    $artistsService = \Drupal::service('spotify.spotify_artists');
    $artist = $artistsService->getArtist($artist_id);
    if (!$artist) {
      $element['#markup'] = '<p>'.$this->t("Sorry, we don't have info on this artist").'</p>';
      return $element;
    }

    $element['#title'] = $artist->name;
    if (!empty($artist->images)) {
      $img = $artist->images[0];
      $element['#markup'] .= '<div class="spotify-image-wrapper">';
      $element['#markup'] .= '<img src="'.$img->url.'" alt="'.$artist->name.'" width="'.$img->width.'" height="'.$img->height.'">';
      $element['#markup'] .= '</div>';
    }

    if (!empty($artist->genres)) {
      $element['#markup'] .= '<ul>';
      foreach ($artist->genres as $genre) {
        $element['#markup'] .= '<li>'.ucwords($genre).'</li>';
      }
      $element['#markup'] .= '</ul>';
    }

    $link = Link::fromTextAndUrl($this->t('Spotify Author Page'), Url::fromUri($artist->external_urls->spotify));
    $link = $link->toRenderable();
    $link['#attributes'] = array('class' => array('button', 'button-action', 'button--primary'));
    $element['#markup'] .= render($link);
    return $element;
  }
}
