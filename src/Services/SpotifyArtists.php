<?php

namespace Drupal\spotify\Services;

class SpotifyArtists {

  protected $settings;
  protected $client;
  protected $cache;
  protected $token;
  protected $artists;

  public function __construct()
  {
    $this->settings = \Drupal::config('spotify.settings');
    $this->client = \Drupal::httpClient();
    $this->cache = \Drupal::service('spotify.spotify_cache');
    $this->run();
  }

  public function getArtist($artistId)
  {
    if(!array_key_exists($artistId, $this->artists)) {
      return false;  
    }
    return $this->artists[$artistId];  
  }

  public function getArtists()
  {
    return $this->artists;
  }

  protected function run()
  {
    $cachedArtists = $this->cache->get('spotify_cahced_data');
    if(!$cachedArtists) {
      $this->authenticate();
      $artists = $this->queryArtistsFromSpotify();
      $this->cache->set('spotify_cahced_data', $artists, time()+86400);
    } else {
      $artists = $cachedArtists->data;
    }
    $this->artists = $artists;
  }

  protected function authenticate()
  {
    $payload = base64_encode($this->settings->get('spotify.spotify_ID') . ':' . $this->settings->get('spotify.spotify_secret'));
    $authString = 'Basic ' . $payload;  
    try {
        $response = $this->client->post('https://accounts.spotify.com/api/token', [
        'verify' => true,
        'form_params' => ['grant_type'=> 'client_credentials'],
            'headers' => [
            'Authorization' => $authString,
            'Content-type' => 'application/x-www-form-urlencoded',
            ],
        ])->getBody()->getContents();
    } catch (\Exception $e) {
        watchdog_exception('spotify', $e);
    }
    $spotifyData = json_decode($response);
    $this->token = $spotifyData->access_token;
  }

  protected function queryArtistsFromSpotify()
  {
    $authString = 'Bearer ' . $this->token;
    try {
      $response = $this->client->get(
        'https://api.spotify.com/v1/search?q=year:2020&type=artist&limit=20',
        ['headers' => [
            'Authorization' => $authString
            ]
        ]
      )->getBody()->getContents();
    } catch (\Exception $e) {
      watchdog_exception('spotify', $e);
    }
    $artists = json_decode($response);
    return $this->prepareForStorage($artists);
  }

  protected function prepareForStorage($artists)
  {
    $storageArray = [];
    foreach ($artists->artists->items as $artist) {
      $storageArray[$artist->id] = $artist;
    }
    return $storageArray;
  }
}