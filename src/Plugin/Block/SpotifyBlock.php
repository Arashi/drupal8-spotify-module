<?php

namespace Drupal\spotify\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a Spotify Authors Block.
 *
 * @Block(
 *   id = "spotify_block",
 *   admin_label = @Translation("Spotify Block"),
 *   category = @Translation("Spotify Block"),
 * )
 */
class SpotifyBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $artistsService = \Drupal::service('spotify.spotify_artists');
    $settings = \Drupal::config('spotify.settings');
    $artists = $artistsService->getArtists();
    if (!$artists) {
       return ['#markup' => $this->t('Ask admin to check Spotify Service!')];
    }

    $maxArtists = $settings->get('spotify.artists_count');
    $index = 1;
    $firstBatch = [];
    $secondBatch = [];
    foreach ($artists as $artist) {
        if ($index <= 10 && $index <= $maxArtists) {
            $firstBatch[] = ['name' => $artist->name, 'id' => $artist->id];
        }
        if ($index > 10 && $index <= $maxArtists) {
            $secondBatch[] = ['name' => $artist->name, 'id' => $artist->id];
        }
        $index++;
    }

    return [
      '#theme' => 'spotify-block-artists',
      '#first_batch' => $firstBatch,
      '#second_batch' => $secondBatch,
      '#attached' => [
        'library' => [
          'spotify/spotify-block',
        ],
      ],
    ];
  }

}
